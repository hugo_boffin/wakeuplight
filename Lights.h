// g b r
// 789 neue pins


class DioderClass {
  public:
    
    void begin() {
      dioderCount = 0;
      
      for(int i = 0; i < maxPins; i++) {
        ledValue[i] = 0;
        ledPins[i] = -1;
      }
    };
                 
    void addRGBStripe(int greenPin, int bluePin, int redPin) {
      ledValue[dioderCount*3+0] = 0;
      ledPins[dioderCount*3+0] = greenPin;
      pinMode(ledPins[dioderCount*3+0], OUTPUT);
      analogWrite(ledPins[dioderCount*3+0], ledValue[dioderCount*3+0]);
      
      ledValue[dioderCount*3+1] = 0;      
      ledPins[dioderCount*3+1] = bluePin;
      pinMode(ledPins[dioderCount*3+1], OUTPUT);
      analogWrite(ledPins[dioderCount*3+1], ledValue[dioderCount*3+1]);
      
      ledValue[dioderCount*3+2] = 0;
      ledPins[dioderCount*3+2] = redPin;
      pinMode(ledPins[dioderCount*3+2], OUTPUT);
      analogWrite(ledPins[dioderCount*3+2], ledValue[dioderCount*3+2]);
      
      dioderCount++;
    };
    
    // TODO: nur bei aenderung
    void update() {
      for(int i = 0; i < dioderCount*3; i++) {
        analogWrite(ledPins[i], ledValue[i]);
      }
    };
    
    void setAllRed(int value) {      
      for(int i = 0; i < dioderCount; i++) {
        setPin(i*3+2, value);
      }

    };
    
    void setAllGreen(int value) {
      for(int i = 0; i < dioderCount; i++) {
        setPin(i*3+0, value);
      }
    };
    
    void setAllBlue(int value) {
      for(int i = 0; i < dioderCount; i++) {
        setPin(i*3+1, value);
      }
    };
    
    void setAllRGB(int value) {      
      for(int i = 0; i < dioderCount*3; i++) {
        setPin(i, value);
      }
    };
    
  private:
    boolean setPin(int i, int newValue) {
      boolean hasChanged = (newValue != ledValue[i]);
      
      if (hasChanged) {
        ledValue[i] = newValue;
        analogWrite(ledPins[i], ledValue[i]);        
      }     
      
      return hasChanged;
    }
    
    static const int maxPins = 12;
    int ledValue[maxPins];
    int ledPins[maxPins];
    int dioderCount;
};


DioderClass Dioder;
