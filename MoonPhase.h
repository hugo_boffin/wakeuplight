#include <math.h>

// Based upon http://www.ben-daglish.net/moon.shtml
// First, a little demonstration.They all return a single value - the phase day(0 to 29, where 0 = new moon, 15 = full etc.) for the selected date.
// http://www.webexhibits.org/calendars/moon-month.html
// http://de.wikipedia.org/wiki/Mondphase
//0auf..15full..ab29

int julday(int year, int month, int day) {
	if (year < 0) { year++; }
	int jy = (year);
	int jm = (month) + 1;
	if (month <= 2) { jy--;	jm += 12; }
	double jul = floor(365.25 *jy) + floor(30.6001 * jm) + (day) + 1720995;
	if (day + 31 * (month + 12 * year) >= (15 + 31 * (10 + 12 * 1582))) {
		double ja = floor(0.01 * jy);
		jul = jul + 2 - ja + floor(0.25 * ja);
	}
	return jul;
}

int Trig2(int day, int month, int year) {
	double n = floor(12.37 * (year - 1900 + ((1.0 * month - 0.5) / 12.0)));
	double RAD = 3.14159265 / 180.0;
	double t = n / 1236.85;
	double t2 = t * t;
	double as = 359.2242 + 29.105356 * n;
	double am = 306.0253 + 385.816918 * n + 0.010730 * t2;
	double xtra = 0.75933 + 1.53058868 * n + ((1.178e-4) - (1.55e-7) * t) * t2;
	xtra += (0.1734 - 3.93e-4 * t) * sin(RAD * as) - 0.4068 * sin(RAD * am);
	int i = (xtra > 0.0 ? floor(xtra) : ceil(xtra - 1.0));
	int j1 = julday(year, month, day);
	int jd = (2415020 + 28 * n) + i;
	return (j1 - jd + 30) % 30;
}

// int light = Trig2(10, 12, 2014);
// double percentage = (15-abs(light - 15))/15.0;
	
