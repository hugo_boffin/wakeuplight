//system libs
#include <LiquidCrystal.h>

//user libs
#define USE_SPECIALIST_METHODS
#include <Time.h>
#include <TimeAlarms.h>
#include <DCF77.h>
#include <dht11.h>

//project code
#include "Utils.h"
#include "Lights.h"
#include "MoonPhase.h"

// TODO: Signalqualität?

enum LightState {
  WakeUpPhase,
  DozeOffPhase,
  IdlePhase
};

enum WakeUpState {
  LightsGoUpPhase,
  BuzzerPhase,
  SnoozePhase
};

enum BottomLeftInfoState {
  DatePhase,
  WeekdayPhase,
  MoonPhase,
  LightPhase
};

enum BottomRightInfoState {
  TempPhase,
  HumidityPhase
};

const unsigned long alarmTimeDuration = 1800000; // 30 Minuten
const unsigned long dozeTimeDuration  = 1800000; // 30 Minuten
const unsigned long snoozeTimeDuration = 300000; // 5 Minuten
const unsigned long autoDisableAlarmTimeDuration = 900000; // 15 Minuten

const int PIN_LED_INTERNAL = 13;

const int DCF_PIN = 18;        // PIN and corresponding interrupt
const int DCF_INTERRUPT = 5;

// Buttons 1..4 from back to front/display
const int PIN_BTN_SNOOZE = 22;
const int PIN_BTN_DEACTIVATE = 24;
const int PIN_BTN_WUTIME = 26;
const int PIN_BTN_DOZE = 28;

const int PIN_SENSOR_DHT11 = 11;

const int PIN_SENSOR_BWM = 12;
const int PIN_SENSOR_FOTO = A14;
int fotoValue = 0;

const int PIN_LED_BACKLIGHT = 30;

int secondCount = 0;

//const int PIN_BUZZER = 40;

LightState lightState = IdlePhase;
WakeUpState wakeUpState = LightsGoUpPhase;

unsigned long stateStartTime;
unsigned long lastSyncTime;

int backLightOnCount = 0;
int klolichtOnCount = -1;

BottomLeftInfoState bottomLeftInfoState = DatePhase;
BottomRightInfoState bottomRightInfoState = TempPhase;

byte grad[8] = {
  B00111,
  B00101,
  B00111,
  B00000,
  B00000,
  B00000,
  B00000,
};

DCF77 DCF = DCF77(DCF_PIN,DCF_INTERRUPT);

dht11 DHT11;
int temperatureVal = 0;
int humidityVal = 0;
int dewPointVal = 0;

AlarmID_t alarmID;

LiquidCrystal lcd(48,49,44,45,46,47); 

void setup() {
  lcd.begin(16,2);
  lcd.clear();  
  lcd.createChar(0, grad);
  
  Serial.begin(9600); 

  pinMode(PIN_BTN_DOZE, INPUT_PULLUP);
  pinMode(PIN_BTN_DEACTIVATE, INPUT_PULLUP);
  pinMode(PIN_BTN_SNOOZE, INPUT_PULLUP);
  pinMode(PIN_BTN_WUTIME, INPUT_PULLUP);
    
//  pinMode(PIN_BUZZER, OUTPUT);
//  digitalWrite(PIN_BUZZER, LOW);
  pinMode(PIN_LED_INTERNAL, OUTPUT);
  digitalWrite(PIN_LED_INTERNAL, LOW);
  
  pinMode(PIN_LED_BACKLIGHT, OUTPUT);
  digitalWrite(PIN_LED_BACKLIGHT, HIGH);
  
  pinMode (PIN_SENSOR_BWM, INPUT);
  pinMode (PIN_SENSOR_FOTO, INPUT);
  
  Dioder.begin();
  //old Dioder.addRGBStripe(5,6,7);
  Dioder.addRGBStripe(7,8,9);
  
  
  DCF.Start();
  Serial.println("Waiting for DCF77 time ... ");
  Serial.println("It will take at least 2 minutes until a first update can be processed.");
  
  setTime(0,0,0, 1,1,2014);
      
  alarmID = Alarm.alarmRepeat(5, 30, 0, alarmFunc);
    
  stateStartTime = 0;
  lastSyncTime = 0;

  lightState = IdlePhase;
  wakeUpState = LightsGoUpPhase;
}

unsigned long clockUpdateInterval = 1000;

void loop() {
  time_t DCFtime = DCF.getTime(); // Check if new DCF77 time is available
  if (DCFtime!=0)
  {
    Serial.println("Time is updated");
        
    if (lastSyncTime == 0) {
      Alarm.disableAll(); // on FirstTimeInit, this would trigger All Daily/... Alarms
      setTime(DCFtime); // Only on first time sync!!!! otherwise it would loose triggers
      Alarm.enableAll();
    } else {
      Alarm.enableAll();
    }
    lastSyncTime = millis();
  }	
  time_t t = now();
  digitalClockDisplay(t);  
  
  updateDHT11();
  
  // Update LCD
  updateDisplay(t);   
  secondCount ++;
  if (secondCount >=5) {
    switch (bottomLeftInfoState) {
    case DatePhase: bottomLeftInfoState = WeekdayPhase; break;
    case WeekdayPhase: bottomLeftInfoState = MoonPhase; break;
    case MoonPhase: bottomLeftInfoState = LightPhase; break;
    case LightPhase: bottomLeftInfoState = DatePhase; break;
    }
    switch (bottomRightInfoState) {
    case HumidityPhase: bottomRightInfoState = TempPhase; break;
    case TempPhase: bottomRightInfoState = HumidityPhase; break;
    }
    secondCount  = 0;
  }
  
  if (backLightOnCount != -1) {
    backLightOnCount ++;
  }
  
  if (klolichtOnCount != -1) {
    klolichtOnCount ++;
  }
  
  fotoValue = analogRead(PIN_SENSOR_FOTO);
  
  unsigned long start = millis();
  while( millis() - start  <= clockUpdateInterval) {
    switch (lightState) {
      case WakeUpPhase:
        doWakeUpPhase();
      break;
      case DozeOffPhase:
        doDozeOffPhase();
        break;
      case IdlePhase:
        {
          
          // Nur hier ist Klolicht aktiv und sinnvoll?          
          int movementDetected = digitalRead(PIN_SENSOR_BWM);
          if (movementDetected == HIGH && fotoValue < 300) {
            klolichtOnCount = 0;
          }
          
          if (klolichtOnCount != -1) {
            Dioder.setAllRed(0);
            Dioder.setAllGreen(0);
            Dioder.setAllBlue(50);
          } else {
            Dioder.setAllRGB(0);          
          }
                  
          if (buttonPressed(PIN_BTN_DOZE)) {
            Serial.println("Starte Doze Phase ... ");
            lightState = DozeOffPhase;
            stateStartTime = millis(); //TODO: StateStartTime, beim Switch über funktion setzen
          }
        }
        //TODO: Hier könnte das klolicht angehen
        break;
    }
    
    checkWakeUpTimeButton(t);
        
    // backligh test    
    if (buttonPressed(PIN_BTN_SNOOZE)) {
      //Start
    }
    
    
    // backlight update
    if (backLightOnCount >= 10) {
       // Stop
      backLightOnCount = -1;
      digitalWrite(PIN_LED_BACKLIGHT, LOW);
    }
    
    if (klolichtOnCount >= 10) {
          klolichtOnCount = -1;
    }
    Alarm.delay(0); // wait one second between clock display
  }
}

void checkWakeUpTimeButton(time_t t) {
  if (buttonPressed(PIN_BTN_WUTIME)) {
    increaseWakeUpTime();
    Alarm.delay(250);
    updateDisplay(t); 
  }
}

void doWakeUpPhase() {
    switch (wakeUpState) {
    case LightsGoUpPhase:
      {
        double timeRunning = ((double(millis() - stateStartTime))/(double(dozeTimeDuration)));
        setWakeUpLights(timeRunning);  
        
        if (timeRunning>=1.0) {
          wakeUpState = BuzzerPhase;
          //digitalWrite(PIN_LED_ALARM, HIGH);
          stateStartTime = millis(); //TODO: StateStartTime, beim Switch über funktion setzen
        }
      }
      break;
    case BuzzerPhase:      
      {
        double timeRunning = ((double(millis() - stateStartTime))/(double(autoDisableAlarmTimeDuration)));

        // TODO: Buzzern
        
        if (buttonPressed(PIN_BTN_SNOOZE))
          wakeUpState = SnoozePhase;      

        if (timeRunning>=1.0) {
          //digitalWrite(PIN_LED_ALARM, LOW);
          lightState = IdlePhase;
          stateStartTime = millis(); //TODO: StateStartTime, beim Switch über funktion setzen
        }
      }
      break;
    case SnoozePhase:
      {
        double timeRunning = ((double(millis() - stateStartTime))/(double(snoozeTimeDuration)));
        if (timeRunning>=1.0) {
          wakeUpState = BuzzerPhase;
          stateStartTime = millis(); //TODO: StateStartTime, beim Switch über funktion setzen
        }
      }
      break;
    };
    
    if (buttonPressed(PIN_BTN_DEACTIVATE))  {
      lightState = IdlePhase;      
      //digitalWrite(PIN_LED_ALARM, LOW);
    }
}

void setWakeUpLights(double progress) {
  int red = int(max(0.0,min(1.0,(progress * 3.0)))*255.0);
  int green = int(max(0.0,min(1.0,((progress-1.0/3.0) * 3.0)))*255.0);
  int blue = int(max(0.0,min(1.0,((progress-2.0/3.0) * 3.0)))*255.0);
  
  Dioder.setAllRed(red);
  Dioder.setAllBlue(blue);
  Dioder.setAllGreen(green);
}

void doDozeOffPhase() {
  double timeRunning = ((double(millis() - stateStartTime))/(double(dozeTimeDuration)));
  setWakeUpLights(1.0-timeRunning);  
  
  if (timeRunning >= 1.0)
    lightState = IdlePhase;      
  
  if (buttonPressed(PIN_BTN_DEACTIVATE))  
    lightState = IdlePhase;      
}

void alarmFunc(){
  Serial.println("Alarmzeit - Weckzeit. Starte Beleuchtung...");     
  stateStartTime = millis(); 
  wakeUpState = LightsGoUpPhase;
  lightState = WakeUpPhase;
}

boolean clockIsInSync() {
  unsigned long delta = millis() - lastSyncTime;
  return (lastSyncTime != 0) && (delta <= 900000);
}


void increaseWakeUpTime() {
  time_t oldTime = Alarm.read(alarmID);
  int h = hour(oldTime);
  int m = minute(oldTime);
  
  m = m + 30;
  
  if (m>=60) {
    m = m - 60;
    h = h + 1;
  }
  
  if (h>=24) {
    h = h - 24;
  }
  
  Serial.print(h);
  Serial.print(":");
  Serial.print(m);
  Serial.print(" ");

  Alarm.free( alarmID ); 
  alarmID = Alarm.alarmRepeat(h, m, 0, alarmFunc);
}

void updateDisplay(time_t t) {
  //0123456789012345
  //00:00:00? >00:00
  //31.12.2014 -20°C
  //Donnerstag -20°C     

  time_t nt = Alarm.getNextTrigger();
  
  lcd.setCursor(0,0);
  printDigits(lcd,hour(t),false);
  printDigits(lcd,minute(t));
  printDigits(lcd,second(t));
  if (!clockIsInSync())  
    lcd.print("?");
  else
    lcd.print(" ");
  
  //lcd.clear();
  
  lcd.setCursor(10,0);
  lcd.print(">");
  printDigits(lcd,hour(nt),false);
  printDigits(lcd,minute(nt));

  // Zeile 2
  lcd.setCursor(0,1);
  switch (bottomLeftInfoState) {
    case DatePhase:
    {
      lcd.print(day(t));
      lcd.print(".");
      lcd.print(month(t));
      lcd.print(".");
      lcd.print(year(t));
    }
    break;
    case WeekdayPhase:
    {
      switch (weekday(t)) {
        case 1: lcd.print("Sonntag"); break;
        case 2: lcd.print("Montag"); break;
        case 3: lcd.print("Dienstag"); break;
        case 4: lcd.print("Mittwoch"); break;
        case 5: lcd.print("Donnerstag"); break;
        case 6: lcd.print("Freitag"); break;
        case 7: lcd.print("Samstag"); break;
        default:lcd.print(weekday(t)); break;
      }
    }
    break;
    case MoonPhase:
    {
      int light = Trig2(day(t), month(t), year(t));
      double percentage = (15-abs(light - 15))/15.0;
      lcd.print(int(percentage *100.0));
      lcd.print("\% ");
      if (light < 15)
        lcd.print("Auf");
      else if (light > 15)
        lcd.print("Ab");
      else
        lcd.print("Voll");
    }
    break;
    case LightPhase:
    {
      double pcntLight = fotoValue/1024.0;   
      lcd.print(int(pcntLight *100.0));
      lcd.print("\% hell");
    }
    break;
  }
  
  lcd.print("          ");
  
  switch (bottomRightInfoState) {
  case HumidityPhase: {
    char tmpstr[8] = "";
    itoa(humidityVal,tmpstr,10);
    
    lcd.setCursor(15-strlen(tmpstr),1);
    lcd.print(humidityVal); 
    lcd.print("%");
  } break;
  case TempPhase: {
    char tmpstr[8] = "";
    itoa(temperatureVal,tmpstr,10);
    
    lcd.setCursor(15-strlen(tmpstr)-1,1);
    lcd.print(temperatureVal); 
    lcd.write(byte(0));
    lcd.print("C");
  } break;
  }
}

boolean buttonPressed(int pin) {
  // Pullup on!
  boolean pressed = (digitalRead(pin) == LOW);
  if (pressed) {
    backLightOnCount = 0;
    digitalWrite(PIN_LED_BACKLIGHT, HIGH);
  }
  return pressed;
}

void updateDHT11() {
  int chk = DHT11.read(PIN_SENSOR_DHT11);
  
  Serial.print("Read sensor: ");
  switch (chk)
  {
    case DHTLIB_OK: 
		Serial.println("OK"); 
		break;
    case DHTLIB_ERROR_CHECKSUM: 
		Serial.println("Checksum error"); 
		break;
    case DHTLIB_ERROR_TIMEOUT: 
		Serial.println("Time out error"); 
		break;
    default: 
		Serial.println("Unknown error"); 
		break;
  }
  
  temperatureVal = DHT11.temperature;
  humidityVal = DHT11.humidity;

  Serial.print("Humidity(%): ");
  Serial.println(DHT11.humidity);

  Serial.print("Temperature(°C): ");
  Serial.println(DHT11.temperature);

  Serial.print("Dew Point(°C): ");
  Serial.println(dewPoint(DHT11.temperature, DHT11.humidity));
}

// dewPoint function NOAA
// reference (1) : http://wahiduddin.net/calc/density_algorithms.htm
// reference (2) : http://www.colorado.edu/geography/weather_station/Geog_site/about.htm
//
double dewPoint(double celsius, double humidity)
{
	// (1) Saturation Vapor Pressure = ESGG(T)
	double RATIO = 373.15 / (273.15 + celsius);
	double RHS = -7.90298 * (RATIO - 1);
	RHS += 5.02808 * log10(RATIO);
	RHS += -1.3816e-7 * (pow(10, (11.344 * (1 - 1/RATIO ))) - 1) ;
	RHS += 8.1328e-3 * (pow(10, (-3.49149 * (RATIO - 1))) - 1) ;
	RHS += log10(1013.246);

        // factor -3 is to adjust units - Vapor Pressure SVP * humidity
	double VP = pow(10, RHS - 3) * humidity;

        // (2) DEWPOINT = F(Vapor Pressure)
	double T = log(VP/0.61078);   // temp var
	return (241.88 * T) / (17.558 - T);
}
